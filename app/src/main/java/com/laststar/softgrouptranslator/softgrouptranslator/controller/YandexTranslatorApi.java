package com.laststar.softgrouptranslator.softgrouptranslator.controller;

import com.laststar.softgrouptranslator.softgrouptranslator.consts.YandexApiConst;
import com.laststar.softgrouptranslator.softgrouptranslator.model.Languages;
import com.laststar.softgrouptranslator.softgrouptranslator.model.TranslateResult;

import java.util.List;

import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by laststar on 05.01.17.
 */

public interface YandexTranslatorApi {
    @GET(YandexApiConst.API + YandexApiConst.VERSION + YandexApiConst.JSON + YandexApiConst.GET_LANGS)
    Observable<Response<Languages>> getLanguages(@Query(YandexApiConst.KEY) String API_KEY, @Query(YandexApiConst.UI) String lang_code);

    @FormUrlEncoded
    @POST(YandexApiConst.API + YandexApiConst.VERSION + YandexApiConst.JSON + YandexApiConst.TRANSLATE)
    Observable<Response<TranslateResult>> translate(@Field(YandexApiConst.KEY) String API_KEY, @Field(YandexApiConst.TEXT) List<String> text
            , @Field(YandexApiConst.LANG) String lang, @Field(YandexApiConst.FORMAT) String format);

    class TranslatorApi {
        public static YandexTranslatorApi create() {
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(YandexApiConst.HOST_NAME)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .build();
            return retrofit.create(YandexTranslatorApi.class);
        }
    }
}
