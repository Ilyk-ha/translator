package com.laststar.softgrouptranslator.softgrouptranslator.view.custom_font.buttons;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.Button;

import com.laststar.softgrouptranslator.softgrouptranslator.R;

/**
 * Created by laststar on 05.01.17.
 */

public class ButtonOpenSansBold extends Button {

    public ButtonOpenSansBold(Context context, AttributeSet attrs){
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets()
                , context.getResources().getString(R.string.open_sans_bold)));
    }
}