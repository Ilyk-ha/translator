package com.laststar.softgrouptranslator.softgrouptranslator.presenter;

import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDoneException;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

import com.laststar.softgrouptranslator.softgrouptranslator.R;
import com.laststar.softgrouptranslator.softgrouptranslator.TranslatorApplication;
import com.laststar.softgrouptranslator.softgrouptranslator.consts.DictionaryDBConts;
import com.laststar.softgrouptranslator.softgrouptranslator.controller.DictionaryDB;
import com.laststar.softgrouptranslator.softgrouptranslator.controller.YandexTranslatorApi;
import com.laststar.softgrouptranslator.softgrouptranslator.consts.YandexApiConst;
import com.laststar.softgrouptranslator.softgrouptranslator.model.Languages;
import com.laststar.softgrouptranslator.softgrouptranslator.model.TranslateResult;
import com.laststar.softgrouptranslator.softgrouptranslator.shared_pref.Save;
import com.laststar.softgrouptranslator.softgrouptranslator.view.mvp.MainActivityMvpView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Response;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by laststar on 05.01.17.
 */

public class MainActivityPresenter implements Presenter<MainActivityMvpView> {
    private MainActivityMvpView mainActivityMvpView;
    private Subscription subscription;
    private TranslateResult translateResult;
    private Languages languages;
    private DictionaryDB dictionaryDB;

    @Override
    public void attachView(MainActivityMvpView mainActivityMvpView) {
        this.mainActivityMvpView = mainActivityMvpView;
        this.dictionaryDB = new DictionaryDB(mainActivityMvpView.getContext());
    }

    @Override
    public void detachView() {
        this.mainActivityMvpView = null;
        if(subscription != null) subscription.unsubscribe();
    }

    public void getLanguages(String yandexAPI_KEY) {
        TranslatorApplication translatorApplication = TranslatorApplication.getContext(mainActivityMvpView.getContext());
        YandexTranslatorApi yandexTranslatorApi = translatorApplication.getYandexTranslatorApi();
        subscription = yandexTranslatorApi.getLanguages(yandexAPI_KEY, "ru")
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(translatorApplication.defaultSubscribeScheduler())
                .subscribe(new Subscriber<Response<Languages>>() {
                    @Override
                    public void onCompleted() {
                        addLanguagesToDB();
                        HashMap<String, String> langMap = Languages.sortByValue(languages.getLangs());
                        langMap.put("", "Определить язык");
                        languages.setLangs(langMap);
                        mainActivityMvpView.getLanguages(languages);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<Languages> languagesResponse) {
                        languages = languagesResponse.body();
                    }
                });
    }

    public void translate(String yandexAPI_KEY, List<String> text, String lang){
        TranslatorApplication translatorApplication = TranslatorApplication.getContext(mainActivityMvpView.getContext());
        YandexTranslatorApi yandexTranslatorApi = translatorApplication.getYandexTranslatorApi();
        subscription = yandexTranslatorApi.translate(yandexAPI_KEY, text, lang, YandexApiConst.FORMAT_PLAIN)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(translatorApplication.defaultSubscribeScheduler())
                .subscribe(new Subscriber<Response<TranslateResult>>() {
                    @Override
                    public void onCompleted() {
                        mainActivityMvpView.translateResult(translateResult, false);
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Response<TranslateResult> translateResultResponse) {
                        translateResult = translateResultResponse.body();
                    }
                });
    }

    public void parserXml(Save save){
        SQLiteDatabase db = dictionaryDB.getWritableDatabase();
        boolean isRusWord = false;
        boolean isEngWord = false;
        boolean isLastEng = false;
        int translate_variants = 1;
        String rus = "";
        String eng = "";
        String query = "INSERT INTO " + DictionaryDBConts.TABLE_DICTIONARY
                + " ("
                + DictionaryDBConts.FIELD_LANG_CODE
                + ", " + DictionaryDBConts.FIELD_TEXT_FROM
                + ", " + DictionaryDBConts.FIELD_TEXT_TO
                + ")"
                + " VALUES (?, ?, ?)";
        db.beginTransaction();
        SQLiteStatement stmt= db.compileStatement(query);
        try {
            XmlPullParser xpp = mainActivityMvpView.getContext().getResources().getXml(R.xml.en_rus);
            while (xpp.getEventType() != XmlPullParser.END_DOCUMENT) {
                switch (xpp.getEventType()) {
                    case XmlPullParser.START_TAG:
                        isEngWord = false;
                        isRusWord = false;
                        if(xpp.getName().equals("entry")){
                            stmt.bindString(1, "en-ru");
                            stmt.bindString(2, eng);
                            stmt.bindString(3, rus);
                            stmt.execute();
                            stmt.clearBindings();
                        }
                        if(xpp.getName().equals("orth")) {
                            isEngWord = true;
                            if(isLastEng){
                                translate_variants++;
                            } else {
                                translate_variants = 1;
                            }
                            isLastEng = true;
                        } else if(xpp.getName().equals("quote")){
                            isRusWord = true;
                            if(!isLastEng){
                                translate_variants++;
                            } else {
                                translate_variants = 1;
                            }
                            isLastEng = false;
                        }
                        break;
                    case XmlPullParser.TEXT:
                        if(isEngWord) {
                            if(translate_variants > 1) {
                                if(!eng.contains(xpp.getText())) {
                                    eng = eng + ", " + xpp.getText();
                                }
                            } else {
                                eng = xpp.getText();
                            }
                        } else if(isRusWord){
                            if(translate_variants > 1) {
                                if(!rus.contains(xpp.getText())) {
                                    rus = rus + ", " + xpp.getText();
                                }
                            } else {
                                rus = xpp.getText();
                            }
                        }
                        break;
                }
                // следующий элемент
                xpp.next();
            }
            stmt.bindString(1, "en-ru");
            stmt.bindString(2, eng);
            stmt.bindString(3, rus);
            stmt.execute();
            stmt.clearBindings();
            db.setTransactionSuccessful();
            db.endTransaction();
            save.saveIsDictionariesInDB();
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadTextFromDictionary(List<String> text, String lang_code){
        List<String> translateText = new ArrayList<>();
        SQLiteDatabase db = dictionaryDB.getWritableDatabase();
        int size = text.size();
        String querySelect = "SELECT " + DictionaryDBConts.FIELD_TEXT_TO
                + " FROM " + DictionaryDBConts.TABLE_DICTIONARY
                + " WHERE " + DictionaryDBConts.FIELD_LANG_CODE + " = ?"
                + " AND " + DictionaryDBConts.FIELD_TEXT_FROM + " = ?"
                + " LIMIT 1";
        SQLiteStatement stmtSelect = db.compileStatement(querySelect);
        db.beginTransaction();
        for(int i=0; i<size; i++) {
            stmtSelect.bindString(1, lang_code);
            stmtSelect.bindString(2, text.get(i));
            try {
                String result = stmtSelect.simpleQueryForString();
                translateText.add(result);
            } catch (SQLiteDoneException exception){
                translateText.add(mainActivityMvpView.getContext().getResources().getString(R.string.not_in_db));
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        stmtSelect.close();
        db.close();
        translateResult = new TranslateResult();
        translateResult.setCode(200);
        translateResult.setText(translateText);
        translateResult.setLang(lang_code);
        mainActivityMvpView.translateResult(translateResult, true);
    }

    public void addTextToDictionary(List<String> text, List<String> translateText, String lang_code){
        SQLiteDatabase db = dictionaryDB.getWritableDatabase();
        int size = text.size();
        String querySelect = "SELECT EXISTS ("
                +"SELECT *"
                + " FROM " + DictionaryDBConts.TABLE_DICTIONARY
                + " WHERE " + DictionaryDBConts.FIELD_LANG_CODE + " = ?"
                + " AND " + DictionaryDBConts.FIELD_TEXT_FROM + " = ?"
                + " AND " + DictionaryDBConts.FIELD_TEXT_TO + " = ?"
                + " LIMIT 1);";
        SQLiteStatement stmtSelect = db.compileStatement(querySelect);
        String queryInsert = "INSERT INTO " + DictionaryDBConts.TABLE_DICTIONARY
                + " ("
                + DictionaryDBConts.FIELD_LANG_CODE
                + ", " + DictionaryDBConts.FIELD_TEXT_FROM
                + ", " + DictionaryDBConts.FIELD_TEXT_TO
                + ")"
                + " VALUES (?, ?, ?)";
        SQLiteStatement stmtInsert = db.compileStatement(queryInsert);
        db.beginTransaction();
        for(int i=0; i<size; i++){
            stmtSelect.bindString(1, lang_code);
            stmtSelect.bindString(2, text.get(i));
            stmtSelect.bindString(3, translateText.get(i));
            long result = stmtSelect.simpleQueryForLong();
            if(result == 0){
                stmtInsert.bindString(1, lang_code);
                stmtInsert.bindString(2, text.get(i));
                stmtInsert.bindString(3, translateText.get(i));
                stmtInsert.execute();
                stmtInsert.clearBindings();
            }
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        stmtSelect.close();
        stmtInsert.close();
        db.close();
    }

    private void addLanguagesToDB(){
        SQLiteDatabase db = dictionaryDB.getWritableDatabase();
        String query = "REPLACE INTO " + DictionaryDBConts.TABLE_LANGUAGES
                + " ("
                + DictionaryDBConts.FIELD_CODE
                + ", " + DictionaryDBConts.FIELD_LANG
                + ")"
                + " VALUES (?, ?)";
        db.beginTransaction();
        SQLiteStatement stmt = db.compileStatement(query);
        for(String key : languages.getLangs().keySet()){
            stmt.bindString(1, key);
            stmt.bindString(2, languages.getLangs().get(key));
            stmt.execute();
            stmt.clearBindings();
        }
        db.setTransactionSuccessful();
        db.endTransaction();
        stmt.close();
        db.close();
    }

    public void getLanguagesFromDB(){
        SQLiteDatabase db = dictionaryDB.getWritableDatabase();
        String query = "SELECT * "
                + " FROM " + DictionaryDBConts.TABLE_LANGUAGES;
        Cursor cursor = db.rawQuery(query, null);
        if(cursor.moveToFirst()){
            HashMap<String, String> languagesArray = new HashMap<>();
            int columnCode = cursor.getColumnIndex(DictionaryDBConts.FIELD_CODE);
            int columnLang = cursor.getColumnIndex(DictionaryDBConts.FIELD_LANG);
            while (cursor.moveToNext()){
                languagesArray.put(cursor.getString(columnCode), cursor.getString(columnLang));
            }
            languages = new Languages();
            HashMap<String, String> langMap = Languages.sortByValue(languagesArray);
            langMap.put("", "Определить язык");
            languages.setLangs(langMap);
        }
        cursor.close();
        db.close();
        mainActivityMvpView.getLanguages(languages);
    }

    public void closeDB(){
        dictionaryDB.close();
    }
}
