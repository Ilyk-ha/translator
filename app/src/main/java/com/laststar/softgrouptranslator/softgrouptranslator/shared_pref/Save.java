package com.laststar.softgrouptranslator.softgrouptranslator.shared_pref;

import android.content.Context;
import android.content.SharedPreferences;

import com.laststar.softgrouptranslator.softgrouptranslator.consts.SharedPrefConst;

/**
 * Created by laststar on 06.01.17.
 */

public class Save {
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;
    private Context context;

    public Save(Context context) {
        this.context = context;
    }

    public void saveIsDictionariesInDB() {
        sharedPreferences = context.getSharedPreferences(SharedPrefConst.SAVE_TO_DB_PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putBoolean(SharedPrefConst.SAVE_TO_BD_KEY, true);
        editor.apply();
    }
}
