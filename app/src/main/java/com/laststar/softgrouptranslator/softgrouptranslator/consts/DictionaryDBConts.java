package com.laststar.softgrouptranslator.softgrouptranslator.consts;

/**
 * Created by laststar on 07.01.17.
 */

public class DictionaryDBConts {
    public static final String DB_NAME = "TranslatorDB";
    public static final String TABLE_DICTIONARY = "dictionary";
    public static final String TABLE_LANGUAGES = "languages";
    public static final String FIELD_CODE = "code";
    public static final String FIELD_LANG = "lang";
    public static final String FIELD_ID = "id";
    public static final String FIELD_LANG_CODE = "lang_code";
    public static final String FIELD_TEXT_FROM = "text_from";
    public static final String FIELD_TEXT_TO = "text_to";
}
