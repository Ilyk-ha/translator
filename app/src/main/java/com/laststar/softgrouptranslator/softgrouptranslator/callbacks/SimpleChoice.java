package com.laststar.softgrouptranslator.softgrouptranslator.callbacks;

/**
 * Created by laststar on 02.01.17.
 */

public interface SimpleChoice {
    void languageIsChosen(String name, String lang_code);
}
