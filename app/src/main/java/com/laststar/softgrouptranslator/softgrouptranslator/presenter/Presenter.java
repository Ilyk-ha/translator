package com.laststar.softgrouptranslator.softgrouptranslator.presenter;

/**
 * Created by laststar on 05.12.16.
 */

public interface Presenter<V> {
    void attachView(V view);
    void detachView();
}