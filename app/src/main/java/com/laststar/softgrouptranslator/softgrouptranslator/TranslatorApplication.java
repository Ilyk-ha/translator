package com.laststar.softgrouptranslator.softgrouptranslator;

import android.app.Application;
import android.content.Context;

import com.laststar.softgrouptranslator.softgrouptranslator.controller.YandexTranslatorApi;

import rx.Scheduler;
import rx.schedulers.Schedulers;

/**
 * Created by laststar on 05.01.17.
 */

public class TranslatorApplication extends Application {
    private YandexTranslatorApi yandexTranslatorApi;
    private Scheduler defaultSubscribeScheduler;

    public static TranslatorApplication getContext(Context context) {
        return (TranslatorApplication) context.getApplicationContext();
    }

    public YandexTranslatorApi getYandexTranslatorApi() {
        if (yandexTranslatorApi == null) {
            yandexTranslatorApi = YandexTranslatorApi.TranslatorApi.create();
        }
        return yandexTranslatorApi;
    }

    public Scheduler defaultSubscribeScheduler() {
        if (defaultSubscribeScheduler == null) {
            defaultSubscribeScheduler = Schedulers.io();
        }
        return defaultSubscribeScheduler;
    }
}
