package com.laststar.softgrouptranslator.softgrouptranslator.view.mvp;

import com.laststar.softgrouptranslator.softgrouptranslator.model.Languages;
import com.laststar.softgrouptranslator.softgrouptranslator.model.TranslateResult;

/**
 * Created by laststar on 05.01.17.
 */

public interface MainActivityMvpView extends MvpView {
    void translateResult(TranslateResult translateResult, boolean isDictionary);
    void getLanguages(Languages languages);
}
