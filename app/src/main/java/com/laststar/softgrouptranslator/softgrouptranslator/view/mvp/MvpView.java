package com.laststar.softgrouptranslator.softgrouptranslator.view.mvp;

import android.content.Context;

/**
 * Created by laststar on 05.12.16.
 */

public interface MvpView {
    Context getContext();
}