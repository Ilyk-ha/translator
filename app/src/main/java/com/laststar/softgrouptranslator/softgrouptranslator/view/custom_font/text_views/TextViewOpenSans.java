package com.laststar.softgrouptranslator.softgrouptranslator.view.custom_font.text_views;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

import com.laststar.softgrouptranslator.softgrouptranslator.R;


/**
 * Created by laststar on 05.01.17.
 */

public class TextViewOpenSans extends TextView{

    public TextViewOpenSans(Context context, AttributeSet attrs){
        super(context, attrs);
        this.setTypeface(Typeface.createFromAsset(context.getAssets()
                , context.getResources().getString(R.string.open_sans)));
    }
}
